package com.litethinking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LitethinkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(LitethinkingApplication.class, args);
	}

}
